import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title : any;
  description : any;
  public adsid : any;
  public imagepath: any;
  public ipaddress: any;
  public link: any;
  public httpOption: any;

  constructor(
    public router: Router,
    public http: HttpClient
  ) {

  }

  ngOnInit() {
    let windowUrl = window.location.href;
    this.link = windowUrl;
    let spliting = windowUrl.split('?');
    let content = spliting[1].split('&');
    this.imagepath = 'http://139.180.129.80:8080/api/Image?' + content[0];
    let splitId = content[1].split('=');
    let splitId2 = content[2].split('=');
    let splitId3 = content[3].split('=');
    this.adsid = parseInt(splitId[1]);
    this.title = splitId2[1];
    this.description = splitId3[1];
    console.log("check", content)
    this.getIPAddress();
    // this.sendShare();
  }

  getIPAddress() {
    this.http.get("http://api.ipify.org/?format=json").subscribe((data) => {
      console.log("data", data);
      let dataIp : any = data
      this.ipaddress = dataIp.ip;
      this.sendShare();
    })
  }

  sendShare() {
    let data = JSON.stringify({
      adsPromoteId: this.adsid,
      ipaddress: this.ipaddress,
      url: this.link
    });
    this.httpOption = {
      headers: new HttpHeaders({
          'Authorization': 'Bearer ' + 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6InNwZWFrZXIiLCJuYW1laWQiOiIzMDEyIiwibmJmIjoxNTc1OTgzMTc5LCJleHAiOjE1NzY1ODc5NzksImlhdCI6MTU3NTk4MzE3OX0.1Pg0tw7FcbkZagK0kmUwKpTHXWLJSWzBkD91aAl-0v8'
      })
    };
    this.http.post('http://139.180.129.80:8080/api/marketer/share', data, this.httpOption).subscribe((data) => {
      console.log("data address", data);
    })
  }

}
